# nextflow pipeline

Faire un génome masqué sans les centromères


# Chip-Seq: peaks de 1 à 2 kb
! attention un des replicats a une couverture plus faible
! seulement 30% des reads map sur le genome (tester macs2 filterdup)

- convetir les bigwig en wig
- faire tourner dpeak sur les wig normalisé

faire tourner Dpeak sur les wig normalisés
Ne pas faire la normalisation quantile de dpeak

Tester Faire tourner Profile et stats sur les deux

# Metaplot autour des peaks de condensine avec la densité moyenne dans le mutant, le WT et le cut3-477
- classification non-superviser des peaks en fonction de leurs hauteurs pour faire plusieurs plots
- au niveau des CDS -> en 3 groupes

# Distribution du nombre de peaks WT, mutant et cut3-477 par 100kb
en faisant attention à ce que les peaks ne soient pas segmentés

# MNASE-Seq:
- Faire tourner le pipe calibré
- injecté le bam du génome d'intéres dans dpos
- appliquer le facteur de normalisation au wig de danpos
- faire tourner dpos sur le wig normalisé

Faire tourner DANPOS à partir des wig normalizé du pipeline de Hu et all
Shifter comme danpos les reads et utiliser Dpos
Ne pas faire la normalisation quantile de dpos

Faire les bw du ratio U200/U50

# Metaplot

méta plot que pour le Chip-Seq pour les ratio, U200 et U50
- Au niveau des TSS (début) +- 1kb
- Au niveau des TTS (fin) +- 1kb

# Distribution des distance au peak suivant entre WT, mutant et cut3-477






