# Material and Methods

All the scripts and pipelines are available in the [gitbio.ens-lyon.fr:LBMC/Bernard/fact_condensin.git](https://gitbio.ens-lyon.fr/LBMC/Bernard/fact_condensin.git) git repository.
The pipelines were executed with [`nextflow`](https://doi.org/10.1038/nbt.3820) (version 21.10.6.5660).

In the subsequent section, *genome* refer to the genome of interest *S. Pombe* while *calibration genome* refer to the calibration genome *S. Cerivisiae*.
We start by processing the fastq file with [`fastp`](https://doi.org/10.1093/bioinformatics/bty560) (version 0.20.1).
Then we concatenate the genome and the calibration genome and prefix every chromosome in the calibration genome with the string `calib_`.
The processed fastq files are then mapped on the index of the concatenated genome with [`bowtie2`](https://doi.org/10.1093/bioinformatics/bty648) (versio 2.3.4.1).
The resulting bam files were then sorted and indexed with [`samtools`]( https://doi.org/10.1093/gigascience/giab008) (version 1.11), before being split into bam of reads mapping exclusively on the genome and bam of reads mapping exclusively on the calibration genome.
We then removed duplicated reads with [`picard`](http://broadinstitute.github.io/picard/) (version 2.18.11).

For convenience, the bam files were then converted to bigwig format using [`deeptools`](https://doi.org/10.1093/nar/gkw257) (version 3.5.1) and bedgraph format.

[Hu et al.](https://doi.org/10.1093/nar/gkv670) proposed a normalization method to remove chip-seq technical bias, by removing observed total read number variability between the calibrated part of the IP samples.
For each bedgraph file, we applied the following transformation to compute $normIP_{xt}$ the normalized read density in the position $t$ of the genome:

$$\text{norm}IP_{i}\left(t\right) = IP_{xi}\left(t\right) \frac{ 10^6 \sum_{t=1}^{T_c}WCE_{ci}\left(t\right)}{\sum_{t=1}^{T_c}IP_{ci}\left(t\right)\sum_{t=1}^{T_x}WCE_{xi}\left(t\right)}$$

With $IP_{xi}\left(t\right)$, the read density at position $t$ in the genome of size $T_x$, and $IP_{ci}\left(t\right)$, the read density at position $t$ in the calibration genome of size $T_c$ for the IP sample $i$ and $WCE_{xi}\left(t\right)$ and $WCE_{ci}\left(t\right)$ the read density for the corresponding WCE sample $i$.

To be able to better analyze the coverage information at repetitive regions of the gnome, we propose to extend the previous normalization to normalize the signal nucleotide by nucleotide and work with the following quantity:

$$\text{ratio}IP_{i}\left(t\right) = \frac{\text{norm}IP_{i}\left(t\right)}{\text{norm}WCE_{i}\left(t\right)}$$

with:

$$\text{norm}WCE_{i\left(t\right)} = WCE_{xi}\left(t\right) \frac{ 10^6 }{\sum_{t=1}^{T_c}WCE_{ci}\left(t\right)} \alpha_i$$

We then find $\alpha_i$ such that:

$$E\left(\text{norm}IP_{i}\left(t\right)\right)= E\left(\frac{\text{norm}IP_{i}\left(t\right)}{\text{norm}WCE_{i}\left(t\right)}\right)$$

$$E\left(\text{norm}WCE_{i}\left(t\right)\right) = 1$$

$$E\left(WCE_{xi}\left(t\right) \frac{ 10^6 }{\sum_{t=1}^{T_c}WCE_{ci}\left(t\right)} \alpha_i\right) = 1$$

$$\frac{1}{T_x}\sum_{t=1}^{T_x}WCE_{xi}\left(t\right) \frac{ 10^6 }{\sum_{t=1}^{T_c}WCE_{ci}\left(t\right)} \alpha_i = 1$$
$$\alpha_i = \frac{1}{\frac{1}{T_x}\sum_{t=1}^{T_x}WCE_{xi}\left(t\right) \frac{ 10^6 }{\sum_{t=1}^{T_c}WCE_{ci}\left(t\right)} }$$

With this method, we retain the interesting properties of [Hu et al.](https://doi.org/10.1093/nar/gkv670) normalization on the average read density between samples (i.e., we can compare two different samples in a quantitative way) and we account for the local bias of read density observed in the WCE samples (differential chromatin accessibility, repetition, low mappability region, etc.).$$
