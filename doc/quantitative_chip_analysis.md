# Quantitative Chip analysis pipeline

## Introduction

This pipeline performs a similar quantitative Chip-Seq analysis as the one described in [Hu et al. 2015](./biblio/Hu_2015.pdf).

The main idea of this approach is to perform a classical Chip-Seq with one difference: we add cells from a second species (calibration cells) to the cells of interest at the start of the experiment.

The principal steps of this analysis are:

1. Reads trimming and adaptor pruning
2. Competitive mapping on a concatenation of the two genomes (reference genome and the calibration genome)
3. split of the alignment results between reads mapping on the reference genome, the calibration genome of both
4. normalization of the results
5. peak calling (optional)

## Launching the pipeline

You can get a full list of the pipeline option with the following command:

```sh
./nextflow src/chip_quant_analyis.nf --help
```

Otherwise a typical call to this pipeline look like the following command:

```sh
./nextflow src/chip_quant_analysis.nf -profile singularity --fasta "data/PB_2020/fasta/S_pombe_ASM294v226.fa" --fasta_calib "data/PB_2020/fasta_calib/S288C_reference_sequence_R64-2-1_20150113.fasta" --csv_path "data/PB_2020/datafromPATH_run1.csv" --macs_gsize 1.25e7 -resume
```

with :
- `--fasta` a path to the reference genome fasta file
- `--fasta_calib` a path to the calibration genome fasta file
- `--csv_path` a csv text file describing the path of the fastq files to process
- `--macs_gsize` an estimate of the reference genome size for `MACS` peak calling


### `--csv_path`

The path of a csv file defining the paths of the fastq files to process. The csv format is the following:


| IP_w;        | WCE_w;        | IP_m;        | WCE_m
---------------|---------------|--------------|--------
| IP_1_w_path; | WCE_1_w_path; | IP_1_m_path; | WCE_1_m_path
| ...          |               |              |
| IP_n_w_path; | WCE_n_w_path; | IP_n_m_path; | WCE_n_m_path


Where :
- The first line is the column name
- `IP_1_w_path`: is the path of the 1st IP fastq in the wild-type condition (use `path_R{1,2}.fastq` to specify paired files)
- `WCE_1_w_path`: is the path of the 1st input fastq in the wild-type condition
- `IP_1_m_path`: is the path of the 1st IP fastq in the mutant condition
- `WCE_1_m_path`: is the path of the 1st input fastq in the mutant condition

### `--csv_sra`
Optionally the `--csv_path` can be replaced with the `--csv_sra` option.
for `--csv_sra`, the format is the same with the path of the fastq replaced by their SRA accession number


## Example

### Command:

```sh
./nextflow src/chip_quant_analysis.nf --macs_gsize 1.25e7 --fasta "data/PB_2020/fasta/S_pombe_ASM294v226.fa" --fasta_calib "data/PB_2020/fasta_calib/S288C_reference_sequence_R64-2-1_20150113.fasta" --csv_path "data/PB_2020/datafromPATH_run1.csv" -profile singularity --macs_mfold "2 100" -resume --perform_peak_calling "true"
```

### Stdout:

```sh
----------------------------------------------------
Genome              : data/PB_2020/fasta/S_pombe_ASM294v226.fa
Genome calibration  : data/PB_2020/fasta_calib/S288C_reference_sequence_R64-2-1_20150113.fasta
CSV_Path            : data/PB_2020/datafromPATH_run1.csv
CSV_SRA             : Not supplied
Remove Duplicate    : True
Peak Calling        : True
MACS2 Genome Size   : 1.25E7
Config Profile      : singularity
----------------------------------------------------
loading local csv files
executor >  local (2)
[0f/728192] process > fastp:fastp_default (T_4483_L1)                                                                                                                [100%] 10 of 10, cached: 10 ✔
[c5/5db1dd] process > mapping:concatenate_genome (S_pombe_ASM294v226 S288C_reference_sequence_R64-2-1_20150113)                                                      [100%] 1 of 1, cached: 1 ✔
[55/e3fa3f] process > mapping:index_fasta (S_pombe_ASM294v226)                                                                                                       [100%] 1 of 1, cached: 1 ✔
[a3/394894] process > mapping:mapping_fastq (IP_4483_L3)                                                                                                             [100%] 10 of 10, cached: 10 ✔
[ea/bfe38a] process > mapping:sort_bam (IP_4483_L3)                                                                                                                  [100%] 10 of 10, cached: 10 ✔
[13/95f03a] process > mapping:filter_bam_mapped (IP_6304_L4)                                                                                                         [100%] 10 of 10, cached: 10 ✔
[f9/613159] process > mapping:filter_bam_multimapped (IP_6304_L2)                                                                                                    [100%] 10 of 10, cached: 10 ✔
[26/24ef60] process > mapping:sort_bam_monomapped (IP_5246_L1)                                                                                                       [100%] 10 of 10, cached: 10 ✔
[fd/967e9f] process > mapping:sort_bam_multimapped (T_4483_L2)                                                                                                       [100%] 10 of 10, cached: 10 ✔
[14/05f4db] process > mapping:filter_bam (T_4483_L1)                                                                                                                 [100%] 10 of 10, cached: 10 ✔
[9f/16cc61] process > mapping:filter_bam_calib (IP_6304_L4)                                                                                                          [100%] 10 of 10, cached: 10 ✔
[8d/88055e] process > mapping:rename_calib_bam (10)                                                                                                                  [100%] 10 of 10, cached: 10 ✔
[f3/d9f1d1] process > mapping:mark_duplicate (T_6304_L1)                                                                                                             [100%] 10 of 10, cached: 10 ✔
[de/0e8334] process > mapping:mark_duplicate_calib (IP_6304_L2)                                                                                                      [100%] 10 of 10, cached: 10 ✔
[4e/c03842] process > mapping:index_bam ([id:IP_6304_L2, mapping:fasta_calib])                                                                                       [100%] 40 of 40, cached: 40 ✔
[88/1a33b4] process > mapping:rename_indexed_bam ([id:IP_5246_L2, mapping:fasta])                                                                                    [100%] 40 of 40, cached: 40 ✔
[a1/ebcf20] process > mapping:group_indexed_bam (IP_5246_L2)                                                                                                         [100%] 10 of 10, cached: 10 ✔
[31/7087da] process > coverage_analysis:stats_bam ([id:IP_5246_L2, mapping:fasta])                                                                                   [100%] 40 of 40, cached: 40 ✔
[7f/2469ca] process > coverage_analysis:rename_mapping_stats (T_6304_L1)                                                                                             [100%] 40 of 40, cached: 40 ✔
[94/beb84e] process > coverage_analysis:mapping_stats (IP_5246_L1)                                                                                                   [100%] 10 of 10, cached: 10 ✔
[6b/bbe2e9] process > coverage_analysis:bam_to_bedgraph ([id:IP_5246_L2, mapping:fasta])                                                                             [100%] 10 of 10, cached: 10 ✔
[14/881ca4] process > coverage_analysis:coverage_normalization ([index:25dd46c44057ca7c50756823dc21929b, group:IP_4483_L3_R2, type:m, WCE:T_5246_L4, IP:IP_5246_L2]) [100%] 8 of 8, cached: 8 ✔
[1d/681a94] process > coverage_analysis:bedgraph_to_bigwig ([index:bdffabfefa36064ddc74975e17bb9bae, group:IP_4483_L3_R2, type:w, WCE:T_4483_L2, IP:IP_4483_L3])     [100%] 8 of 8, cached: 8 ✔
[6b/cc12a5] process > coverage_analysis:normalization_stats ([index:acb66715cb35012a633e36b3ba4890ff, group:IP_4483_L1_R1, type:w, WCE:T_4483_L1, IP:IP_4483_L1])    [100%] 8 of 8, cached: 8 ✔
[ea/e91bae] process > peak_calling:peak_calling_macs2 ([index:25dd46c44057ca7c50756823dc21929b, group:IP_4483_L3_R2, type:w, IP:IP_4483_L3, WCE:T_4483_L2])          [ 17%] 1 of 6
[-        ] process > peak_calling:merge_peaks                                                                                                                       -
[-        ] process > peak_calling:peak_calling_quantification                                                                                                       -
[a2/2814e0] process > flagstat_2_multiqc ([id:T_6304_L1, mapping:fasta])                                                                                             [100%] 40 of 40, cached: 40 ✔
[-        ] process > multiqc                                                                                                                                        -
```

### `results/`

```
results
├── coverage
│   ├── bigwig
│   │   ├── IP_4483_L1.bw
│   │   ├── IP_4483_L1_norm.bw
│   │   ├── ...
│   └── stats
│       ├── IP_4483_L1_bg_OR.tsv
│       ├── IP_4483_L1_R1.pdf
│       ├── ...
├── genome
│   ├── bed
│   │   ├── S288C_reference_sequence_R64-2-1_20150113.bed
│   │   └── S_pombe_ASM294v226.bed
│   └── fasta
│       └── S_pombe_ASM294v226_S288C_reference_sequence_R64-2-1_20150113.fasta
├── mapping
│   ├── bam
│   │   ├── IP_4483_L1
│   │   │   ├── IP_4483_L1_all.bam
│   │   │   ├── IP_4483_L1_all.bam.bai
│   │   │   ├── IP_4483_L1_both.bam
│   │   │   ├── IP_4483_L1_both.bam.bai
│   │   │   ├── IP_4483_L1_fasta.bam
│   │   │   ├── IP_4483_L1_fasta.bam.bai
│   │   │   ├── IP_4483_L1_fasta_calib.bam
│   │   │   └── IP_4483_L1_fasta_calib.bam.bai
│   │   │   └── ...
│   └── stats
│       ├── IP_4483_L1.pdf
│       ├── IP_4483_L1.tsv
│       ├── ...
├── peaks
│   ├── IP_4483_L1.bed
│   ├── IP_4483_L1_sorted_mapped_monomapped_sorted_filtered_dedup_peaks.narrowPeak
│   ├── ...
├── QC
│   ├── multiqc_data
│   │   ├── multiqc_bowtie2.txt
│   │   ├── multiqc_data.json
│   │   ├── multiqc_fastp.txt
│   │   ├── multiqc_general_stats.txt
│   │   ├── multiqc.log
│   │   ├── multiqc_macs.txt
│   │   ├── multiqc_picard_dups.txt
│   │   └── multiqc_sources.txt
│   └── multiqc_report.html
└── report.html
```