#!/usr/bin/env nextflow

/*
./nextflow src/chip_quant_analysis.nf --macs_gsize 1.25e7 --fasta "data/PB_2020/fasta/S_pombe_ASM294v226.fa" --fasta_calib "data/PB_2020/fasta_calib/S288C_reference_sequence_R64-2-1_20150113.fasta" --csv_path "data/PB_2020/datafromPATH_run1.csv" -profile singularity --macs_mfold "2 100" -resume

./nextflow src/chip_quant_analysis.nf --macs_gsize 1.25e7 --fasta "data/PB_2020/fasta/S_pombe_ASM294v226.fa" --fasta_calib "data/PB_2020/fasta_calib/S288C_reference_sequence_R64-2-1_20150113.fasta" --csv_path "data/PB_2020/datafromPATH_run1.csv" -profile psmn --macs_mfold "2 100" -resume

./nextflow src/chip_quant_analysis.nf --macs_gsize 1.25e7 --fasta "data/PB_2020/fasta/S_pombe_ASM294v226.fa" --fasta_calib "data/PB_2020/fasta_calib/S288C_reference_sequence_R64-2-1_20150113.fasta" --csv_path "data/PB_2020/datafromPATH_run1_single.csv" -profile singularity --macs_mfold "2 100" -resume

cp data/PB_2020/fasta/S_pombe_ASM294v226.fa data/PB_2020/fasta/S_pombe_ASM294v226_bis.fa
./nextflow src/chip_quant_analysis.nf --macs_gsize 1.25e7 --fasta "data/PB_2020/fasta/S_pombe_ASM294v226.fa" --fasta_calib "data/PB_2020/fasta/S_pombe_ASM294v226_bis.fa" --csv_path "data/PB_2020/datafromPATH_run1.csv" -profile singularity --macs_mfold "2 100" -resume
*/

/*
========================================================================================
                         QUANTITATIVE chip_analysis
========================================================================================
*/
nextflow.enable.dsl=2

def helpMessage() {
  log.info"""
  Usage:

  The typical command for running the pipeline is as follows:
    ./nextflow src/chip_quant_analysis.nf --csv_path data/datafromPath.csv --fasta data/fasta.fasta --fasta_calib data/fasta_calib.fasta -profile singularity

  Fastq files:
    --csv_[path|sra]              Full path of a csv file defining the path or SRA of the fastq files to process. The csv format is the following:
    IP_w; WCE_w; IP_m; WCE_m
    IP_1_w_path; WCE_1_w_path; IP_1_m_path; WCE_1_m_path
    ...
    IP_n_w_path; WCE_n_w_path; IP_n_m_path; WCE_n_m_path

    Where :
    - The first line is the column name
    - IP_1_w_path: is the path of the 1th IP fastq in the wild-type condition (use path_R{1,2}.fastq to specify paired files)
    - WCE_1_w_path: is the path of the 1th input fastq in the wild-type condition
    - IP_1_m_path: is the path of the 1th IP fastq in the mutant condition
    - WCE_1_m_path: is the path of the 1th input fastq in the mutant condition
    for --csv_sra, the format is the same with the path of the fastq replaced by their SRA accession number

  Reference genomes
    --fasta                       Full path to directory containing genome fasta file
    --fasta_calib                 Full path to directory containing genome fasta file used for calibration

  Peak Calling :

  Nextflow config:
    -profile                      Profil used by nextflow to run the pipeline (you have choice between singularity, docker, psmn or ccin2p3)
                                  For local utilisation use singularity or docker

  Remove duplicates (optional)
    --perform_mark_duplicate     Activate reads duplicates removal step (default: "true")

  Peak calling (optional)
    --perform_peak_calling "false"     Desactivate peak calling step (default: "true")
    --macs_gsize                       Effective genome size (if not specified macs2 will not run)
    --macs_mfold "2 100"               Optional set minimal and maximal coverage fold difference to detect a peak

  help message:
    --help                        Print help message

  example:
  ./nextflow src/chip_quant_analysis.nf --macs_gsize 1.25e7 --fasta "data/PB_2020/fasta/S_pombe_ASM294v226.fa" --fasta_calib "data/PB_2020/fasta_calib/S288C_reference_sequence_R64-2-1_20150113.fasta" --csv_path "data/PB_2020/datafromPATH_run1.csv" -profile singularity -resume
  """
  .stripIndent()
}

////////////////////////////////////////////////////
/* --         DEFAULT PARAMETER VALUES         -- */
////////////////////////////////////////////////////

params.perform_mark_duplicate = true
params.perform_peak_calling = true
params.help = false
params.csv_path = ""
params.csv_sra = ""
params.fasta= false
params.fasta_calib= false
params.macs_gsize = false
params.index_fasta_out = ""

// Show help message
if (params.help) {
    helpMessage()
    exit 0
}


// Header log info
def summary = [:]
summary['Genome']                 = params.fasta ?: 'Not supplied'
summary['Genome calibration']     = params.fasta_calib ?: 'Not supplied'
summary['CSV_Path']         = params.csv_path.size() > 0 ? params.csv_path : 'Not supplied'
summary['CSV_SRA']         = params.csv_sra.size() > 0 ? params.csv_sra : 'Not supplied'
summary['Remove Duplicate']       = params.perform_mark_duplicate != false ? "True" : 'Skipped'
summary['Peak Calling']       = params.perform_peak_calling != false ? "True" : 'Skipped'
summary['MACS2 Genome Size']      = params.macs_gsize ?: 'Not supplied'
summary['expected calibration ratio'] = params.expected_ratio
summary['Config Profile']         = workflow.profile
log.info summary.collect { k,v -> "${k.padRight(20)}: $v" }.join("\n")
log.info "-\033[2m--------------------------------------------------\033[0m-"


// Show a big warning message if we're not running MACS
if (!params.macs_gsize) {
  log.error """
=============================================================
  WARNING! MACS genome size parameter not precised
  Peak calling analysis will be skipped.
  Please specify value for '--macs_gsize' to run these steps.
=============================================================
"""
}

Channel
  .value(params.macs_gsize)
  .set{ macs_gsize }

Channel
  .fromList(evaluate(params.metagene_group))
  .set{ metagene_group }

Channel
  .fromList(evaluate(params.dilution_group))
  .set{ dilution_group }

Channel
  .fromPath( params.rm_from_genome, checkIfExists: true )
  .map { it -> [it.simpleName, it]}
  .set{ rm_from_genome }

Channel
  .fromPath( params.gff, checkIfExists: true )
  .map { it -> [it.simpleName, it]}
  .set{ gff_files }

Channel
  .fromPath( params.fasta, checkIfExists: true )
  .map { it -> [it.simpleName, it]}
  .ifEmpty { log.error """
=============================================================
  WARNING! No genome fasta file precised.
  Use --fasta <my genome> 
  Or --help for more informations
=============================================================
""" }
  .set { fasta }

Channel
  .fromPath( params.fasta_calib, checkIfExists: true )
  .map { it -> [it.simpleName, it]}
  .ifEmpty { log.error """
=============================================================
  WARNING! No Calibration genome fasta file precised.
  Use '--fasta_calib'
  Or '--help' for more informations
=============================================================
""" }
  .set { fasta_calib }

def file_name_to_path (file_name) {
  Channel
    .fromFilePairs(file_name, size: -1)
    .ifEmpty { log.error """
=============================================================
  ERROR! problem in the csv file format with ${file_name}
=============================================================
""" }
  .first()
  .getVal()
}

/*
===============================================================================
                         shared modules
===============================================================================
*/

include {
  csv_parsing;
} from './csv_parsing.nf'

params.coverage = ""
params.size = ""
include {
  sample_fastq 
} from './nf_modules/rasusa/main' addParams(
  sample_fastq_coverage: params.coverage,
  sample_fastq_size: params.size
)

include {
  fastp
} from './nf_modules/fastp/main'

include {
  mapping;
} from './mapping.nf'

include {
  coverage_analysis;
} from './mnase_coverage_analysis.nf' addParams(
  expected_ratio: params.expected_ratio
)

include {
  danpos;
} from './mnase_danpos.nf' addParams(
  metagene_plot_out: "dpos/",
  dpos_out: "dpos/"
)

include {
  peak_calling;
} from './peak_calling.nf'

include { flagstat_2_multiqc } from './nf_modules/samtools/main'
include { multiqc } from './nf_modules/multiqc/main' addParams(
  multiqc_out: "QC/mnaseseq/"
)


/*
===============================================================================
                         Workflow
===============================================================================
*/

workflow {
  // QC
  csv_parsing()
  if (params.csv_sra.size() > 0) {
    fastq_dump(csv_parsing.out.map{ it -> it.file }.unique())
    sample_fastq(csv_parsing.out.map{ it -> it.file }.unique(), fasta.collect())
    fastp(sample_fastq.out.fastq)
  } else {
    sample_fastq(csv_parsing.out.map{ it -> it.file }.unique(), fasta.collect())
    fastp(sample_fastq.out.fastq)
  }

  // mapping
  mapping(
    fasta.collect(),
    fasta_calib.collect(),
    fastp.out.fastq,
    csv_parsing.out
  )

  // coverage analysis
  coverage_analysis(
    mapping.out.bam,
    mapping.out.bam_idx,
    csv_parsing.out,
    mapping.out.fasta,
    mapping.out.bed_fasta,
    mapping.out.bed_fasta_calib,
    fastp.out.fastq,
    rm_from_genome.collect(),
  )

  // /* peak calling */
  // peak_calling(
  //   mapping.out.bam,
  //   csv_parsing.out,
  //   coverage_analysis.out.bg
  // )

  danpos(
    fastp.out.fastq,
    mapping.out.fasta,
    mapping.out.bed_fasta,
    coverage_analysis.out.bw,
    coverage_analysis.out.bg,
    gff_files,
    dilution_group,
    metagene_group
  )

  // /* quality control */

  fastp.out.report
    .mix(
      mapping.out.report,
      coverage_analysis.out.report,
      // peak_calling.out.report,
    )
    .set { multiqc_report }
  multiqc(
    multiqc_report
  )
}
