/* ========================= modules import =================================*/
include { fastq_dump } from './nf_modules/sratoolkit/main'

/* ================= helper function =============================*/

def ref_order = {
  // check paired file order
  if (it[1][0].simpleName =~ /R1/) {
    it[1][0].simpleName
  } else {
    it[1][1].simpleName
  }
}

def read_order = {
  // check paired file order
  if (it[4][0].simpleName =~ /R1/) {
    [it[4][0].simpleName[0..-4], [it[4][0], it[4][1]]]
  } else {
    [it[4][1].simpleName[0..-4], [it[4][1], it[4][0]]]
  }
}

/* ========================= workflow =================================*/
workflow csv_parsing {
  if (params.csv_path.size() > 0) {
    log.info "loading local csv files"
    Channel
      .fromPath(params.csv_path, checkIfExists: true)
      .splitCsv(header: true, sep: ";", strip: true)
      .flatMap{
        it -> [
          [(it.IP_w + it.WCE_w + it.IP_m + it.WCE_m).md5(), file(it.IP_w, checkIfExists: true), "IP", "w", file(it.IP_w, checkIfExists: true)],
          [(it.IP_w + it.WCE_w + it.IP_m + it.WCE_m).md5(), file(it.IP_w, checkIfExists: true), "WCE", "w", file(it.WCE_w, checkIfExists: true)],
          [(it.IP_w + it.WCE_w + it.IP_m + it.WCE_m).md5(), file(it.IP_w, checkIfExists: true), "IP", "m", file(it.IP_m, checkIfExists: true)],
          [(it.IP_w + it.WCE_w + it.IP_m + it.WCE_m).md5(), file(it.IP_w, checkIfExists: true), "WCE", "m", file(it.WCE_m, checkIfExists: true)]
        ]
      }
      .map{ it ->
        if (it[1] instanceof List){
          it
        } else {
          [it[0], [it[1]], it[2], it[3], [it[4]]]
        }
      }
      .map{
        it ->
        if (it[1].size() == 2){ // if data are paired_end
          [
            "index": it[0],
            "group": ref_order(it),
            "ip": it[2],
            "type": it[3],
            "id": read_order(it)[0],
            "file": read_order(it)
          ]
        } else {
          [
            "index": it[0],
            "group": it[1][0].simpleName,
            "ip": it[2],
            "type": it[3],
            "id": it[4][0].simpleName,
            "file": [it[4][0].simpleName, it[4]]
          ]
        }
      }
      .set{input_csv}
  } else {
    log.info "loading remotes SRA csv files"
    Channel
      .fromPath(params.csv_sra, checkIfExists: true)
      .splitCsv(header: true, sep: ";", strip: true)
      .flatMap{
        it -> [
          [[it.IP_w + it.WCE_w + it.IP_m + it.WCE_m], it.IP_w, "IP", "w", it.IP_w],
          [[it.IP_w + it.WCE_w + it.IP_m + it.WCE_m], it.IP_w, "WCE", "w", it.WCE_w],
          [[it.IP_w + it.WCE_w + it.IP_m + it.WCE_m], it.IP_w, "IP", "m", it.IP_m],
          [[it.IP_w + it.WCE_w + it.IP_m + it.WCE_m], it.IP_w, "WCE", "m", it.WCE_m]
        ]
      }
      .map{
        it ->
        if (it[1].size() == 2){ // if data are paired_end
          [
            "index": (
              it[0][0][0].simpleName +
              it[0][0][1].simpleName +
              it[0][0][2].simpleName +
              it[0][0][3].simpleName
            ).md5(),
            "group": it[1][0].simpleName,
            "ip": it[2],
            "type": it[3],
            "id": it[4][0].simpleName[0..-4],
            "file": [it[4][0].simpleName[0..-4], it[4]]
          ]
        } else {
          [
            "index": (
              it[0][0].simpleName +
              it[0][1].simpleName +
              it[0][2].simpleName +
              it[0][3].simpleName
            ).md5(),
            "group": it[1].simpleName,
            "ip": it[2],
            "type": it[3],
            "id": it[4].simpleName,
            "file": [it[4].simpleName, it[4]]
          ]
        }
      }
      .set{input_csv}
  }
  emit:
  input_csv
}
