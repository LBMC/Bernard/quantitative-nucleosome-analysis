#!/usr/bin/Rscript
library("tidyverse")
library("vroom")

# compute OR value
load_stat <- function(file) {
  vroom(
    file,
    col_names = c("value", "name", "type"),
    col_types = list("d", "f", "f")
  ) %>%
  filter(str_detect(type, "fasta.*")) %>%
  filter(str_detect(name, "^mapped$"))
}

or_data <- tibble(file = list.files()) %>%
  filter(str_detect(file, ".*\\.tsv")) %>%
  mutate(sample = ifelse(str_detect(file, "^IP_.*"), "ip", "w")) %>%
  mutate(mapping_stats = map(file, load_stat)) %>%
  unnest(c(mapping_stats))


extract_value <- function(data, sample_name, type_name) {
  data %>%
    filter(sample == sample_name & type == type_name) %>%
    pull(value)
}

get_or <- function(or_data, file, expected_ratio) {
  ip_x <- or_data %>% extract_value("ip", "fasta")
  ip_c <- or_data %>% extract_value("ip", "fasta_calib")
  correction <- (ip_x * expected_ratio) / (ip_c - expected_ratio * ip_c)
  or <- 10e6 / (ip_c * correction)

  vroom_write(
    tibble(
      fasta_calib = c(
        ip_c,
        or_data %>% extract_value("w", "fasta_calib")
      ),
      fasta = c(
        ip_x,
        or_data %>% extract_value("w", "fasta")
      ),
      expected_ratio_calib = c(expected_ratio, expected_ratio),
      correction = c(correction, correction),
      type = c("ip", "w"),
      or = c(or, or)
    ),
    path = str_c(file, "_OR.tsv")
  )
  return(or)
}

# normalize bg files

norm_bg <- function(file, or) {
    vroom(
      file,
      col_names = c("chr", "start", "stop", "density"),
      col_types = list("f", "i", "i", "d")
    ) %>%
    mutate(density = density * or) %>%
    vroom_write(
      path = str_c("norm_", file),
      col_names = FALSE
    )
    return(0)
}

args = commandArgs(trailingOnly=TRUE)
expected_ratio = as.numeric(args[1])

bg_files <- tibble(file = list.files()) %>%
  filter(str_detect(file, ".*\\.bg$")) %>%
  mutate(
    mapping_stats = map(file, function(x, or_data, expected_ratio) {
      norm_bg(x, get_or(or_data, x, expected_ratio))
    }, or_data = or_data, expected_ratio = expected_ratio)
  )

