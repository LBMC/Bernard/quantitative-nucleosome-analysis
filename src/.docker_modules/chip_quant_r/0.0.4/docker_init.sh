#!/bin/sh
docker pull lbmc/chip_quant_r:0.0.4
docker build src/.docker_modules/chip_quant_r/0.0.4 -t 'lbmc/chip_quant_r:0.0.4'
docker push lbmc/chip_quant_r:0.0.4
