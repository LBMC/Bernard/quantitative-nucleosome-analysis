#!/usr/bin/env python3

import sys


design=sys.argv[1]
## same as design input but make sure there is no space in the raw, avoid error in nextflow process
design_checked=sys.argv[2]



def csv_parser(csv, csv_out):
    ERROR_STR = 'ERROR: Please check design file\n'
    def check_line_format(lspl):
        ## CHECK VALID NUMBER OF COLUMNS PER SAMPLE
        numCols = len(lspl)
        if numCols not in [2]:
            print("Invalid nb colonne")
            #print (f"{ERROR_STR}Invalid number of columns (should be 2)!\nLine: {lspl}\nWARN: Colomn separator must be \t")
            sys.exit(1)

        IP,INPUT = lspl[0],lspl[1]
        if str(IP)==str(INPUT):
            print("same file ip and input")
            #print (f"{ERROR_STR}Same file specified as IP and INPUT!\nLine: {lspl}\n")
            sys.exit(1)
        return IP,INPUT

    HEADER = ['ip', 'input']

    header = csv.readline().strip().split('\t')
    if header != HEADER:
        print("not good header")
        #print (f"{ERROR_STR} header:{header}!= {HEADER}\nWARN: Colomn separator must be \t")
        sys.exit(1)
    #csv_out.write(f"ip\tinput\n")
    csv_out.write("ip"+"\t"+"input\n")
    sampleMappingDict = {}
    for line in csv:
        line_sple = [elt.strip() for elt in line.strip().split("\t")]
        IP,INPUT=check_line_format(line_sple)
        IP=str(IP)
        INPUT=str(INPUT)
        ## CHECK UNICITY COUPLE [IP,INPUT]
        #couple=f"{IP};{INPUT}"
        couple=str(IP+";"+INPUT)
        if couple not in sampleMappingDict:
            sampleMappingDict[couple]=""
        else:
            print ("WARN: couple IP vs INPUT specified multiple times")
            #print (f"WARN: couple IP vs INPUT specified multiple times, migth be an error)!\nLine: {line_sple}")
            sys.exit(1)
        #csv_out.write(f"{IP}\t{INPUT}\n")
        csv_out.write(str(IP)+"\t"+str(INPUT)+"\n")
    return None



if __name__ == "__main__":
    # execute only if run as a script
    try:
        design_f=open(design,'r')
    except IOError:
        print('Cannot open', design)
        exit()
    try:
        output_f=open(design_checked,'w')
    except IOError:
        print('Cannot open', design_checked)
        exit()

    csv_parser(design_f,output_f)
