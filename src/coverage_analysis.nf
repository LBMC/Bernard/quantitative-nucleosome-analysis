/* ========================= modules import =================================*/
include {
  bam_to_bedgraph
} from './nf_modules/bedtools/main'

include {
  stats_bam;
  rm_from_bam;
} from './nf_modules/samtools/main'

include {
  bam_to_bigwig
} from './nf_modules/deeptools/main' addParams(bam_to_bigwig_out: "coverage/raw_bw/")

include {
  gff_to_bed
} from './nf_modules/agat/main' addParams(
  gff_to_bed: "--sub gene"
)

def filter_metagen(file_id, metagen_list) {
  for(library in metagen_list) {
    if(file_id.matches(".*" + library + ".*"))
      return true
  }
  return false
}

def pick_metagen(file_id, metagen_list) {
  for(library in metagen_list) {
    if(file_id.matches(".*" + library + ".*"))
      return library
  }
  return null
}


/* ========================= processes =================================*/
process rename_mapping_stats {
  tag "$file_id"
  input:
    tuple val(file_id), val(stats_label), path(stats)

  output:
    tuple val(file_id), path("*.tsv"), emit: tsv

  script:
"""
ln -s ${stats} ${stats_label}.tsv
"""
}

process mapping_stats {
  container = "lbmc/chip_quant_r:0.0.6"
  label 'big_mem_mono_cpus'
  tag "$file_id"
  publishDir "results/mapping/stats/", mode: 'copy'

  input:
    tuple val(file_id), path(stats)

  output:
    tuple val(file_id), path("${file_id}.pdf"), emit: pdf
    tuple val(file_id), path("${file_id}.tsv"), emit: tsv

  script:
  if (file_id instanceof List){
    file_prefix = file_id[0]
  } else {
    file_prefix = file_id
  }
"""
mapping_stats.R

mv plot.pdf ${file_prefix}.pdf
mv stats.tsv ${file_prefix}.tsv
"""
}

process coverage_normalization {
  container = "lbmc/chip_quant_r:0.0.6"
  label 'big_mem_mono_cpus'
  tag "${file_id}"
  publishDir "results/coverage/stats/", mode: 'copy', pattern: '*.tsv'


  input:
    tuple val(file_id), path(WCE_tsv), path(IP_tsv), path(WCE_bg), path(IP_bg)

  output:
    tuple val(file_id), path("${WCE_bg}"), path("norm_${WCE_bg.simpleName}.bg"), path("${IP_bg}"), path("norm_${IP_bg.simpleName}.bg"), emit: bg
    tuple val(file_id), path("*.tsv"), emit: or

  script:
"""
mv ${WCE_tsv} WCE_${WCE_tsv}
mv ${IP_tsv} IP_${IP_tsv}

normalization.R

mv ${WCE_bg.baseName}.bg_OR.tsv ${file_id.WCE}_bg_OR.tsv
mv ${IP_bg.baseName}.bg_OR.tsv ${file_id.IP}_bg_OR.tsv
"""
}


process coverage_stats {
  container = "lbmc/chip_quant_r:0.0.6"
  label 'big_mem_mono_cpus'
  tag "${file_id}"
  publishDir "results/coverage/stats/", mode: 'copy', pattern: '*.tsv'


  input:
    file files

  output:
    path("*.tsv"), emit: stats

  script:
"""
parse_stats.R
"""
}

process coverage_ratio {
  container = "lbmc/chip_quant_r:0.0.6"
  label 'big_mem_mono_cpus'
  tag "${file_id}"
  publishDir "results/coverage/bigwig/", mode: 'copy'

  input:
    tuple val(file_id), path(norm_WCE_bw), path(norm_IP_bw)

  output:
    tuple val(file_id), path("ratio_${norm_IP_bw}"), emit: bw

  script:
"""
bw_ratio.R ${norm_IP_bw} ${norm_WCE_bw} &> \
  ${norm_IP_bw.simpleName}_ratio_report.txt
if grep -q "Error" ${norm_IP_bw.simpleName}_ratio_report.txt; then
  exit 1
fi
"""
}

process bedgraph_to_bigwig {
  container = "lbmc/ucsc:407"
  label "big_mem_mono_cpus"
  tag "${file_id}"
  publishDir "results/coverage/bigwig/", mode: 'copy'

  input:
    tuple val(file_id), path(bg_w), path(bg_w_norm), path(bg_ip), path(bg_ip_norm)
    tuple val(bed_id), path(bed_fasta)
    tuple val(bed_calib_id), path(bed_fasta_calib)

  output:
  tuple val(file_id), path("${file_id.WCE}_norm.bw"), path("${file_id.IP}_norm.bw"), emit: bw

  script:
"""
LC_COLLATE=C

# transform bed file of start-stop chromosome size to stop chromosome size
cat ${bed_fasta} ${bed_fasta_calib} | \
  awk -v OFS="\\t" '{print \$1, \$3}' > chromsize.txt

sort -T ./ -k1,1 -k2,2n ${bg_w} > sort_${bg_w}
bedGraphToBigWig sort_${bg_w} \
  chromsize.txt \
  ${file_id.WCE}.bw
rm sort_${bg_w}

sort -T ./ -k1,1 -k2,2n ${bg_w_norm} > sort_${bg_w_norm}
bedGraphToBigWig sort_${bg_w_norm} \
  chromsize.txt \
  ${file_id.WCE}_norm.bw
rm sort_${bg_w_norm}

sort -T ./ -k1,1 -k2,2n ${bg_ip} > sort_${bg_ip}
bedGraphToBigWig sort_${bg_ip} \
  chromsize.txt \
  ${file_id.IP}.bw
rm sort_${bg_ip}

sort -T ./ -k1,1 -k2,2n ${bg_ip_norm} > sort_${bg_ip_norm}
bedGraphToBigWig sort_${bg_ip_norm} \
  chromsize.txt \
  ${file_id.IP}_norm.bw
rm sort_${bg_ip_norm}
"""
}

process normalization_stats {
  container = "lbmc/chip_quant_r:0.0.6"
  label 'big_mem_mono_cpus'
  tag "$file_id"
  publishDir "results/coverage/stats/", mode: 'copy'

  input:
    tuple val(file_id), path(bg_w), path(bg_w_norm), path(bg_ip), path(bg_ip_norm)

  output:
    tuple val(file_id), path("${file_id.group}.pdf"), emit: pdf

  script:
"""
mv ${bg_w} W_${file_id.group}_${file_id.WCE}.bg
mv ${bg_w_norm} W_${file_id.group}_${file_id.WCE}_norm.bg
mv ${bg_ip} IP_${file_id.group}_${file_id.IP}.bg
mv ${bg_ip_norm} IP_${file_id.group}_${file_id.IP}_norm.bg
normalization_stats.R
mv plot.pdf ${file_id.group}.pdf
"""
}

process deeptools_plot {
  container = "lbmc/deeptools:3.5.1"
  label "big_mem_multi_cpus"
  tag "$title"
  publishDir "results/coverage/", mode: 'copy'

  input:
    tuple val(title), val(groups), val(pairs_names), path(bw_ip)
    tuple val(bed_id), path(bed)

  output:
    tuple val(title), path("*.plotProfile.pdf"), emit: pdf
    tuple val(title), path("${title}.matrix.mat.gz"), emit: matrix

  script:
"""
computeMatrix scale-regions \
        --regionsFileName ${bed} \
        --scoreFileName ${bw_ip} \
        --outFileName ${title}.computeMatrix.mat.gz \
        --outFileNameMatrix ${title}.matrix.mat.gz \
        --regionBodyLength 4000 \
        --beforeRegionStartLength 2000 \
        --afterRegionStartLength 2000 \
        --skipZeros \
        --samplesLabel ${groups.join(" ")} \
        --numberOfProcessors ${task.cpus} \
        &> ${title}_compute_matrix_report.txt
plotProfile --matrixFile ${title}.computeMatrix.mat.gz \
    --outFileName ${title}.plotProfile.pdf \
    --perGroup \
    --plotTitle ${title} \
    &> ${title}_compute_matrix_report.txt
"""
}

process metagen_plot {
  container = "lbmc/chip_quant_r:0.0.6"
  label 'big_mem_mono_cpus'
  tag "$file_id"
  publishDir "results/coverage/", mode: 'copy'

  input:
    tuple val(file_id), path(matrix)

  output:
    tuple val(file_id), path("metagen_${matrix.simpleName}.pdf"), emit: pdf
    tuple val(file_id), path("metagen_${matrix.simpleName}_log.pdf"), emit: log_pdf

  script:
"""
metagen.R ${matrix}
mv ${matrix}.pdf metagen_${matrix.simpleName}.pdf
mv ${matrix}_log.pdf metagen_${matrix.simpleName}_log.pdf
"""
}

/* ========================= workflow =================================*/
workflow coverage_analysis {
  take:
    bam
    bam_idx
    input_csv
    fasta
    bed_fasta
    bed_fasta_calib
    rm_from_genome
    metagene_group
    gff

  main:
  bam_to_bigwig(
    bam_idx
    .filter{ it[0].mapping != 'both' }
    .filter{ it[0].mapping != 'all' }
  )
  stats_bam(
    bam
  )
  rename_mapping_stats(
    stats_bam.out.tsv
    .map {
      it -> [it[0].id, it[0].mapping, it[1]]
    }
  )
  mapping_stats(
    rename_mapping_stats.out.tsv
      .groupTuple(by: 0)
      .map{
        it ->
        if (it[0] instanceof List){
          [it[0][0], it[1]]
        } else {
          it
        }
      }
  )

  rm_from_bam(
    bam
      .filter {it[0].mapping == "fasta"},
    rm_from_genome.collect()
  )
  bam_to_bedgraph(
    rm_from_bam.out.bam
  )

  bam_to_bedgraph.out.bedgraph
  .map{ it -> [it[0].id, it[1]] }
  .map{
    it ->
    if (it[0] instanceof List){
      [it[0][0], it[1]]
    } else {
      it
    }
  }
  .join(
    mapping_stats.out.tsv
  )
  .join(
    input_csv
    .map{ it -> [it.id, it] }
  )
  .map{ it -> [
    "index": it[3].index,
    "id": it[3].id,
    "group":it[3].group,
    "ip": it[3].ip,
    "type": it[3].type,
    "tsv": it[2],
    "bg": it[1]
    ]
  }
  .map{ it -> [it.index, it.type, it]}
  .groupTuple(by: [0, 1])
  .map{ it -> it[2]}
  .map{ it ->
    if (it[0].ip == "IP") {
      [
        [
          "index": it[0].index,
          "group": it[0].group,
          "type": it[0].type,
          "WCE": it[1].id,
          "IP": it[0].id,
        ],
        it[1].tsv,
        it[0].tsv,
        it[1].bg,
        it[0].bg
      ]
    } else {
      [
        [
          "index": it[0].index,
          "group": it[0].group,
          "type": it[0].type,
          "WCE": it[0].id,
          "IP": it[1].id,
        ],
        it[0].tsv,
        it[1].tsv,
        it[0].bg,
        it[1].bg
      ]
    }
  }
  .set{ bed_graph_OR }

  coverage_normalization(
    bed_graph_OR,
  )
  
  coverage_stats(
    coverage_normalization.out.or
      .map{
        it -> it[1]
      }
      .collect()
  )


  bedgraph_to_bigwig(
    coverage_normalization.out.bg,
    bed_fasta.collect(),
    bed_fasta_calib.collect()
  )

  coverage_ratio(
    bedgraph_to_bigwig.out.bw
  )

  normalization_stats(
    coverage_normalization.out.bg
  )


  gff_to_bed(gff)
  bedgraph_to_bigwig.out.bw
    .map{
      it -> [it[0], it[2]]
    }
    .toSortedList( { a, b -> b[0].IP <=> a[0].IP } )
    .flatMap()
    .combine(
      metagene_group
    )
    .map{
      it -> [it[2], it[3], it[0], it[1]]
    }
    .filter{
      filter_metagen(it[2].IP, it[1])
    }
    .map{
      it -> [it[0], it[2].IP, pick_metagen(it[2].IP, it[1]), it[3]]
    }
    .groupTuple(by: [0, 1])
    .groupTuple(by: 0)
    .map{
      it -> [
        it[0],
        it[2].flatten(),
        it[1].flatten(),
        it[3].flatten()
      ]
    }
    .set{ bw_plots }

  bedgraph_to_bigwig.out.bw
    .map{
      it -> [it[0], it[1]]
    }
    .toSortedList( { a, b -> b[0].IP <=> a[0].IP } )
    .flatMap()
    .combine(
      metagene_group
    )
    .map{
      it -> [it[2], it[3], it[0], it[1]]
    }
    .filter{
      filter_metagen(it[2].IP, it[1])
    }
    .map{
      it -> [it[0], it[2].IP, pick_metagen(it[2].IP, it[1]), it[3]]
    }
    .groupTuple(by: [0, 1])
    .groupTuple(by: 0)
    .map{
      it -> [
        "wce_" + it[0],
        it[2].flatten(),
        it[1].flatten(),
        it[3].flatten()
      ]
    }
    .set{ wce_bw_plots }

  coverage_ratio.out.bw
    .map{
      it -> [it[0], it[1]]
    }
    .toSortedList( { a, b -> b[0].IP <=> a[0].IP } )
    .flatMap()
    .combine(
      metagene_group
    )
    .map{
      it -> [it[2], it[3], it[0], it[1]]
    }
    .filter{
      filter_metagen(it[2].IP, it[1])
    }
    .map{
      it -> [it[0], it[2].IP, pick_metagen(it[2].IP, it[1]), it[3]]
    }
    .groupTuple(by: [0, 1])
    .groupTuple(by: 0)
    .map{
      it -> [
        "ratio_" + it[0],
        it[2].flatten(),
        it[1].flatten(),
        it[3].flatten()
      ]
    }
    .set{ ratio_bw_plots }

  deeptools_plot(
    bw_plots
      .mix(wce_bw_plots)
      .mix(ratio_bw_plots),
      gff_to_bed.out.bed.collect()
  )
  metagen_plot(deeptools_plot.out.matrix)

  emit:
    bg = coverage_normalization.out.bg
    bw = bedgraph_to_bigwig.out.bw
    report = stats_bam.out.report
}