/* ========================= modules import =================================*/

include {
  dpos_bw_no_b
} from './nf_modules/danpos/main' addParams(
  dpos: "--smooth_width 0 -n N ",
  dpos_out: "dpos"
)
include {
  gff_to_bed
} from './nf_modules/agat/main' addParams(
  gff_to_bed: "--sub gene"
)

include {
  bedgraph_to_bigwig
} from './nf_modules/ucsc/main'


def filter_metagen(file_id, metagen_list) {
  for(library in metagen_list) {
    if(file_id.matches(".*" + library + ".*"))
      return true
  }
  return false
}

def pick_metagen(file_id, metagen_list) {
  for(library in metagen_list) {
    if(file_id.matches(".*" + library + ".*"))
      return library
  }
  return null
}

def update_library(library_list, file_list) {
  repeat = file_list.size()
  result = []
  for (i in 0..<repeat) {
    for (j in 0..<file_list[i].size()) {
      result.add(library_list[i])
    }
  }
  return result.flatten()
}

/* ========================= processes =================================*/

process dilution_normalization {
  container = "lbmc/ucsc:407"
  label 'big_mem_mono_cpus'
  tag "${file_id}"
  publishDir "results/dpos/ratio/${file_prefix}/", mode: 'copy', pattern: '*.bg'


  input:
    tuple val(file_id), path(dilution_a_bg), path(dilution_b_bg)
    tuple val(bed_id), path(bed)

  output:
    tuple val(file_id), path("${file_id[0]}_${file_id[1]}.bg"), emit: bg
    tuple val(file_id), path("${file_id[0]}_${file_id[1]}.bw"), emit: bw

  script:
  switch(file_id) {
    case {it instanceof List}:
      file_prefix = file_id[0]
    break
    case {it instanceof Map}:
      file_prefix = file_id.values()[0]
    break
    default:
      file_prefix = file_id
    break
  }
"""
join -j2 -o 1.1,1.2,1.3,1.4,2.4 --nocheck-order \
  ${dilution_a_bg} ${dilution_b_bg} | \
  awk '{print \$1, \$2, \$3, (\$4 + 1)/(\$5 + 1)}' > \
  ${file_id[0]}_${file_id[1]}.bg

LC_COLLATE=C
# transform bed file of start-stop chromosome size to stop chromosome size
awk -v OFS="\t" '{print \$1, \$3}' ${bed} > chromsize.txt
bedGraphToBigWig ${file_id[0]}_${file_id[1]}.bg chromsize.txt ${file_id[0]}_${file_id[1]}.bw
"""
}

process deeptools_plot_TSS {
  container = "lbmc/deeptools:3.5.1"
  label "big_mem_multi_cpus"
  tag "$title"
  if (params.metagene_plot_out != "") {
    publishDir "results/${params.metagene_plot_out}", mode: 'copy'
  }

  input:
    tuple val(title), val(groups), path(bw_ip)
    tuple val(bed_id), path(bed)

  output:
    tuple val(title), path("*.plotProfile.pdf"), emit: pdf
    tuple val(title), path("${title}.TSS.matrix.mat.gz"), emit: matrix

  script:
"""
computeMatrix reference-point \
        --referencePoint TSS \
        --regionsFileName ${bed} \
        --scoreFileName ${bw_ip} \
        --outFileName ${title}.TSS.computeMatrix.mat.gz \
        --outFileNameMatrix ${title}.TSS.matrix.mat.gz \
        --beforeRegionStartLength 1000 \
        --afterRegionStartLength 1000 \
        --skipZeros \
        --samplesLabel ${groups.join(" ")} \
        --numberOfProcessors ${task.cpus} \
        &> ${title}_TSS_compute_matrix_report.txt
plotProfile --matrixFile ${title}.TSS.computeMatrix.mat.gz \
    --outFileName ${title}.TSS.plotProfile.pdf \
    --perGroup \
    --plotTitle ${title} \
    &> ${title}_TSS_compute_matrix_report.txt
"""
}

process deeptools_plot_TES {
  container = "lbmc/deeptools:3.5.1"
  label "big_mem_multi_cpus"
  tag "$title"
  if (params.metagene_plot_out != "") {
    publishDir "results/${params.metagene_plot_out}", mode: 'copy'
  }

  input:
    tuple val(title), val(groups), path(bw_ip)
    tuple val(bed_id), path(bed)

  output:
    tuple val(title), path("*.plotProfile.pdf"), emit: pdf
    tuple val(title), path("${title}.TES.matrix.mat.gz"), emit: matrix

  script:
"""
computeMatrix reference-point \
        --referencePoint TES \
        --regionsFileName ${bed} \
        --scoreFileName ${bw_ip} \
        --outFileName ${title}.TES.computeMatrix.mat.gz \
        --outFileNameMatrix ${title}.TES.matrix.mat.gz \
        --beforeRegionStartLength 1000 \
        --afterRegionStartLength 1000 \
        --skipZeros \
        --samplesLabel ${groups.join(" ")} \
        --numberOfProcessors ${task.cpus} \
        &> ${title}_TES_compute_matrix_report.txt
plotProfile --matrixFile ${title}.TES.computeMatrix.mat.gz \
    --outFileName ${title}.TES.plotProfile.pdf \
    --perGroup \
    --plotTitle ${title} \
    &> ${title}_TES_compute_matrix_report.txt
"""
}


/* ========================= workflow =================================*/

workflow danpos {
  take:
    fastq
    fasta
    bed_fasta
    bw
    bg
    gff
    dilution_group
    metagene_group

  main:
  gff_to_bed(gff)
  dpos_bw_no_b(
    fasta.collect(),
    fastq.collect(),
    bw
    .map{
      it -> [it[0], it[2]]
    }
  )
  dilution_group
    .map{ it -> [it]}
    .combine(
      bg
      .unique{ it[0].id }
      .map{ it -> [it[0].id, [it[0], [it[4]]]] }
    )
    .filter{ filter_metagen(it[1], it[0]) }
    .groupTuple(by: 0)
    .map{
      it ->
      if ( filter_metagen(it[1][0], [it[0][0]])) { // dilution order is the same
      [
        it[1],
        it[2][1][1][0],
        it[2][0][1][0]
      ]

      } else {
      [
        [it[1][1], it[1][0]],
        it[2][0][1][0],
        it[2][1][1][0]
      ]
      }
    }
    .set{ dilution_norm }

  dilution_normalization(
    dilution_norm,
    bed_fasta.collect()
  )
  
  dilution_normalization.out.bw
    .map{
      it -> [it[0].join('_'), it[1]]
    }
    .mix(
      bw
      .map{
        it -> [it[0].ip, it[2]]
      }
    )
    .toSortedList()
    .flatMap()
    .combine(
      metagene_group
    )
    .map{
      it -> [it[0], it[2], it[3], it[1]]
    }
    .filter{
      filter_metagen(it[0], it[2])
    }
    .toSortedList()
    .flatMap()
    .unique{it[0]}
    .map{
      it -> [it[1], it[0], it[3]]
    }
    .groupTuple(by: [0, 1])
    .groupTuple(by: 0)
    .map{
      it -> [
        it[0],
        it[1].flatten(),
        it[2].flatten(),
      ]
    }
    .set{ bw_plots }
  
  deeptools_plot_TSS(bw_plots, gff_to_bed.out.bed.collect())
  deeptools_plot_TES(bw_plots, gff_to_bed.out.bed.collect())

}