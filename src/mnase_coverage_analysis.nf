/* ========================= modules import =================================*/
include {
  bam_to_bedgraph
} from './nf_modules/bedtools/main'

include {
  stats_bam;
  rm_from_bam;
} from './nf_modules/samtools/main'

include { dpos_bam_bg } from './nf_modules/danpos/main' addParams(
  dpos_out: "dpos/",
)

include {
  bam_to_bigwig
} from './nf_modules/deeptools/main' addParams(bam_to_bigwig_out: "coverage/raw_bw/")


/* ========================= processes =================================*/
process rename_mapping_stats {
  tag "$file_id"
  input:
    tuple val(file_id), val(stats_label), path(stats)

  output:
    tuple val(file_id), path("*.tsv"), emit: tsv

  script:
"""
ln -s ${stats} ${stats_label}.tsv
"""
}

process mapping_stats {
  container = "lbmc/chip_quant_r:0.0.5"
  label 'big_mem_mono_cpus'
  tag "$file_id"
  publishDir "results/mapping/stats/", mode: 'copy'

  input:
    tuple val(file_id), path(stats)

  output:
    tuple val(file_id), path("${file_id}.pdf"), emit: pdf
    tuple val(file_id), path("${file_id}.tsv"), emit: tsv

  script:
  if (file_id instanceof List){
    file_prefix = file_id[0]
  } else {
    file_prefix = file_id
  }
"""
mapping_stats.R
mv plot.pdf ${file_prefix}.pdf
mv stats.tsv ${file_prefix}.tsv
"""
}

process coverage_normalization {
  container = "lbmc/chip_quant_r:0.0.5"
  label 'big_mem_mono_cpus'
  tag "${file_id}"
  publishDir "results/coverage/stats/", mode: 'copy', pattern: '*.tsv'


  input:
    tuple val(file_id), path(WCE_tsv), path(IP_tsv), path(WCE_bg), path(IP_bg)

  output:
    tuple val(file_id), path("${WCE_bg}"), path("norm_${WCE_bg.simpleName}.bg"), path("${IP_bg}"), path("norm_${IP_bg.simpleName}.bg"), emit: bg
    tuple val(file_id), path("*.tsv"), emit: or

  script:
"""

mv ${WCE_tsv} WCE_${WCE_tsv}
mv ${IP_tsv} IP_${IP_tsv}
normalization_mnase.R ${params.expected_ratio}
mv ${WCE_bg.baseName}.bg_OR.tsv ${file_id.wce}_bg_OR.tsv
mv ${IP_bg.baseName}.bg_OR.tsv ${file_id.ip}_bg_OR.tsv
"""
}

process bedgraph_to_bigwig {
  container = "lbmc/ucsc:407"
  label "big_mem_mono_cpus"
  tag "${file_id}"
  publishDir "results/coverage/bigwig/", mode: 'copy'

  input:
    tuple val(file_id), path(bg_w), path(bg_w_norm), path(bg_ip), path(bg_ip_norm)
    tuple val(bed_id), path(bed_fasta)
    tuple val(bed_calib_id), path(bed_fasta_calib)

  output:
  tuple val(file_id), path("${file_id.wce}_norm.bw"), path("${file_id.ip}_norm.bw"), emit: bw

  script:
"""
LC_COLLATE=C

# transform bed file of start-stop chromosome size to stop chromosome size
cat ${bed_fasta} ${bed_fasta_calib} | \
  awk -v OFS="\\t" '{print \$1, \$3}' > chromsize.txt

sort -T ./ -k1,1 -k2,2n ${bg_w} > sort_${bg_w}
bedGraphToBigWig sort_${bg_w} \
  chromsize.txt \
  ${file_id.wce}.bw
rm sort_${bg_w}

sort -T ./ -k1,1 -k2,2n ${bg_w_norm} > sort_${bg_w_norm}
bedGraphToBigWig sort_${bg_w_norm} \
  chromsize.txt \
  ${file_id.wce}_norm.bw
rm sort_${bg_w_norm}

sort -T ./ -k1,1 -k2,2n ${bg_ip} > sort_${bg_ip}
bedGraphToBigWig sort_${bg_ip} \
  chromsize.txt \
  ${file_id.ip}.bw
rm sort_${bg_ip}

sort -T ./ -k1,1 -k2,2n ${bg_ip_norm} > sort_${bg_ip_norm}
bedGraphToBigWig sort_${bg_ip_norm} \
  chromsize.txt \
  ${file_id.ip}_norm.bw
rm sort_${bg_ip_norm}
"""
}

process normalization_stats {
  container = "lbmc/chip_quant_r:0.0.5"
  label 'big_mem_mono_cpus'
  tag "$file_id"
  publishDir "results/coverage/stats/", mode: 'copy'

  input:
    tuple val(file_id), path(bg_w), path(bg_w_norm), path(bg_ip), path(bg_ip_norm)

  output:
    tuple val(file_id), path("${file_id.group}.pdf"), emit: pdf

  script:
"""
mv ${bg_w} W_${file_id.group}_${file_id.wce}.bg
mv ${bg_w_norm} W_${file_id.group}_${file_id.wce}_norm.bg
mv ${bg_ip} IP_${file_id.group}_${file_id.ip}.bg
mv ${bg_ip_norm} IP_${file_id.group}_${file_id.ip}_norm.bg
normalization_stats.R
mv plot.pdf ${file_id.group}.pdf
"""
}


/* ========================= workflow =================================*/
workflow coverage_analysis {
  take:
    bam
    bam_idx
    input_csv
    fasta
    bed_fasta
    bed_fasta_calib
    fastq
    rm_from_genome

  main:
  rm_from_bam(
    bam
      .filter {it[0].mapping == "fasta"},
    rm_from_genome.collect()
  )
  bam_to_bigwig(bam_idx)
  stats_bam(
    bam
  )
  rename_mapping_stats(
    stats_bam.out.tsv
    .map {
      it -> [it[0].id, it[0].mapping, it[1]]
    }
  )
  mapping_stats(
    rename_mapping_stats.out.tsv
      .groupTuple(by: 0)
  )
  dpos_bam_bg(
    fasta.collect(),
    fastq.collect(),
    rm_from_bam.out.bam
      .filter {it[0].mapping == "fasta"}
      .map{ it -> [it[0].id, it[1]] }
      .combine(
        input_csv
          .map{ it -> [it.id, it] },
        by: 0
      )
      .map{
        it -> 
        [it[2].index, it[2].type, it[2], it[1]]
      }
      .groupTuple(by: [0, 1])
      .map { it -> 
        if (it[2][0].ip == "IP") {
          [
            [
              "id": it[2][0].id,
              "ip": it[2][0].id,
              "wce": it[2][1].id,
              "type": it[2][0].type,
              "group": it[2][0].group,
              "index": it[2][0].index
            ],
            it[3][0],
            it[3][1]
          ]
        } else {
          [
            [
              "id": it[2][1].id,
              "ip": it[2][1].id,
              "wce": it[2][0].id,
              "type": it[2][1].type,
              "group": it[2][1].group,
              "index": it[2][1].index
            ],
            it[3][1],
            it[3][0]
          ]
        }
      }
  )

  dpos_bam_bg.out.bg
  .map{ it -> [it[0].ip, it] }
  .combine(
    mapping_stats.out.tsv,
    by: 0
  )
  .map{ it -> [it[1][0].wce, [it[1][0], it[2], it[1][1], it[1][2]]] }
  .combine(
    mapping_stats.out.tsv,
    by: 0
  )
  .map{ it -> [it[1][0], it[2], it[1][1], it[1][3], it[1][2]] }
  .set{ bed_graph_OR }

  coverage_normalization(
    bed_graph_OR,
  )

  bedgraph_to_bigwig(
    coverage_normalization.out.bg,
    bed_fasta.collect(),
    bed_fasta_calib.collect()
  )

  normalization_stats(
    coverage_normalization.out.bg
  )

  emit:
    bg = coverage_normalization.out.bg
    bw = bedgraph_to_bigwig.out.bw
    report = stats_bam.out.report
}