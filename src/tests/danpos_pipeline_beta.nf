#!/usr/bin/env nextflow

/*
 * Defines some parameters in order to specify the refence genomes
 * and read pairs by using the command line options
 */


params.fastq="/home/narudo/ENS_stage/data/Tonelli/*/*{7,8,9}_fastq*"
params.fasta = "/home/narudo/ENS_stage/GCA_000002945.2/GCA_000002945.2_ASM294v2_genomic._sansMitoch.fna"
params.outdir = 'results_Toselli'


 log.info """\
          D A N P O S   P I P E L I N E
          =============================
          fastq: ${params.fastq}
          genome: ${params.fasta}
          outdir: ${params.outdir}
          """
          .stripIndent()

Channel
  .fromPath( params.fasta )
  .ifEmpty { error "Cannot find any bam files matching: ${params.fasta}" }
  .set { fasta_file }
Channel
  .fromPath( params.fastq )
  .ifEmpty { error "Cannot find any fastq files matching: ${params.fastq}" }
  .map { it -> [(it.baseName =~ /([^\.]*)/)[0][1], it]}
  .set { fastq_files }

 /*
  * Builds the genome index required by the mapping process
  */

process index_fasta {
  tag "$fasta.baseName"
  label "bowtie2"

  input:
    file fasta from fasta_file

  output:
    file "*.index*" into index_files
    file "*_report.txt" into indexing_report

  script:
  """
  bowtie2-build --threads ${task.cpus} ${fasta} ${fasta.baseName}.index &> ${fasta.baseName}_bowtie2_report.txt

  if grep -q "Error" ${fasta.baseName}_bowtie2_report.txt; then
    exit 1
  fi
  """
}


 /*
  *  Maps each fastq by using bowtie mapper tool
  */
process bowtie2 {
  tag "$file_id"
  label "bowtie2"

  input:
  set file_id, file(reads) from fastq_files
  file index from index_files.collect()

  output:
  file "*.bam" into bam_files
  file "*_report.txt" into mapping_report

  script:
  index_id = index[0]
  for (index_file in index) {
    if (index_file =~ /.*\.1\.bt2/ && !(index_file =~ /.*\.rev\.1\.bt2/)) {
        index_id = ( index_file =~ /(.*)\.1\.bt2/)[0][1]
    }
  }
  """
  bowtie2 --very-sensitive -p ${task.cpus} -x ${index_id} \
  -U ${reads} 2> \
  ${file_id}_bowtie2_report_tmp.txt | \
  samtools view -Sb - > ${file_id}.bam

  if grep -q "Error" ${file_id}_bowtie2_report_tmp.txt; then
    exit 1
  fi
  tail -n 19 ${file_id}_bowtie2_report_tmp.txt > ${file_id}_bowtie2_report.txt
  """
}


/*
 *  Calling peaks with DANPOS tools
 */

process danpos {
  label "danpos"
  publishDir "${params.outdir}", mode: 'copy'

  input:
  file(bams) from bam_files.collect()

  output:
  //file "*" into danpos_files
  file "sample.smooth.wig" into xls_file
  file "sample.smooth.positions.xls" into wig_file
  //file "report.txt" into danpos_report

  script:
  """
  danpos.py dpos -a 1 -n N ../* && \
  cp result/pooled/*.wig sample.smooth.wig && \
  cp result/pooled/*.xls sample.smooth.positions.xls
  """
}
