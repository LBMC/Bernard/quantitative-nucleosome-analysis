#!/usr/bin/env nextflow


/*
 * Defines some parameters in order to specify the refence genomes
 * and read pairs by using the command line options
 */

params.genome = "/home/narudo/ENS_stage/GCA_000002945.2/GCA_000002945.2_ASM294v2_genomic.fna"
params.outdir = 'results'


log.info """\
         I N D E X I N G   G E N O M E
         =============================
         genome: ${params.genome}
         outdir: ${params.outdir}
         """
         .stripIndent()

 Channel
   .fromPath( params.genome )
   .ifEmpty { error "Cannot find any index files matching: ${params.genome}" }


/*
 * Builds the genome index required by the mapping process
 */
process buildIndex {
    tag "$genome.baseName"
    publishDir "$params.outdir/mapping/index/", mode: 'copy'


    input:
    path genome from params.genome

    output:
    //path 'genome.index*' into index_ch
    path "${genome.baseName}*" into index_ch


//  bowtie-build ${genome} genome.index
    """
    bowtie-build ${genome} ${genome.baseName}
    """
}
