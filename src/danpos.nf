/* ========================= modules import =================================*/

include {
  dpeak_bw
} from './nf_modules/danpos/main' addParams(
  dpeak: "--smooth_width 0 -n N ",
  dpeak_out: "dpeak/"
)
include {
  wig2_to_bigwig2
} from './nf_modules/ucsc/main'
include {
  gff_to_bed;
  gff_to_gtf
} from './nf_modules/agat/main' addParams(
  gff_to_bed: "--sub gene"
)

def filter_metagen(file_id, metagen_list) {
  for(library in metagen_list) {
    if(file_id.matches(".*" + library + ".*"))
      return true
  }
  return false
}

def pick_metagen(file_id, metagen_list) {
  for(library in metagen_list) {
    if(file_id.matches(".*" + library + ".*"))
      return library
  }
  return null
}

def update_library(library_list, file_list) {
  repeat = file_list.size()
  result = []
  for (i in 0..<repeat) {
    for (j in 0..<file_list[i].size()) {
      result.add(library_list[i])
    }
  }
  return result.flatten()
}

/* ========================= processes =================================*/

process deeptools_plot {
  container = "lbmc/deeptools:3.5.1"
  label "big_mem_multi_cpus"
  tag "$title"
  if (params.metagene_plot_out != "") {
    publishDir "results/${params.metagene_plot_out}", mode: 'copy'
  }

  input:
    tuple val(title), val(groups), val(pairs_names), path(bw_ip)
    tuple val(bed_id), path(bed)

  output:
    tuple val(title), path("*.plotProfile.pdf"), emit: pdf
    tuple val(title), path("${title}.matrix.mat.gz"), emit: matrix

  script:
"""
computeMatrix scale-regions \
        --regionsFileName ${bed} \
        --scoreFileName ${bw_ip} \
        --outFileName ${title}.computeMatrix.mat.gz \
        --outFileNameMatrix ${title}.matrix.mat.gz \
        --regionBodyLength 4000 \
        --beforeRegionStartLength 2000 \
        --afterRegionStartLength 2000 \
        --skipZeros \
        --samplesLabel ${groups.join(" ")} \
        --numberOfProcessors ${task.cpus} \
        &> ${title}_compute_matrix_report.txt
    plotProfile --matrixFile ${title}.computeMatrix.mat.gz \
        --outFileName ${title}.plotProfile.pdf \
        --perGroup \
        --plotTitle ${title} \
        &> ${title}_compute_matrix_report.txt
"""
}


/* ========================= workflow =================================*/
workflow danpos {
  take:
    fastq
    fasta
    bw
    gff
    metagene_group

  main:
  gff_to_bed(gff)
  dpeak_bw(
    fasta.collect(),
    fastq.collect(),
    bw
    .map{
      it -> [[it[0].IP, it[0]], it[2], it[1]] // [[ids,...,ids], wce, ip] ->[[ids,...,ids], ip, wce] 
    }
  )
  wig2_to_bigwig2(
    fasta,
    dpeak_bw.out.wig
      .map{
        it -> [it[0][1], it[1], it[2]]
      }
  )

  wig2_to_bigwig2.out.bw
    .map{
      it -> [it[0], it[1]]
    }
    .toSortedList( { a, b -> b[0].IP <=> a[0].IP } )
    .flatMap()
    .combine(
      metagene_group
    )
    .map{
      it -> [it[2], it[3], it[0], it[1]]
    }
    .filter{
      filter_metagen(it[2].IP, it[1])
    }
    .map{
      it -> [it[0], it[2].IP, pick_metagen(it[2].IP, it[1]), it[3]]
    }
    .groupTuple(by: [0, 1])
    .groupTuple(by: 0)
    .map{
      it -> [
        it[0],
        it[2].flatten(),
        it[1].flatten(),
        it[3].flatten()
      ]
    }
    .set{ bw_plots }
  
  deeptools_plot(bw_plots, gff_to_bed.out.bed.collect())
}